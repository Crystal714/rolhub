class AddGameToGroups < ActiveRecord::Migration[5.1]
  def change
    add_reference :groups, :game, foreign_key: true
  end
end
