class CreateGroup < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :player_limit
      t.string :game_frequency, null: true
      t.datetime :schedule
      t.string :communication
      t.string :support, null: true
      t.string :player_level, null: true
    end
    add_index :groups, :name, unique: true
  end
end
