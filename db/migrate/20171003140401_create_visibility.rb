class CreateVisibility < ActiveRecord::Migration[5.1]
  def change
    create_table :visibilities do |t|
      t.string :name
    end
    add_index :visibilities, :name, unique: true
  end
end
