class AddCustomFieldToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string
    add_index :users, :username, unique: true
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :birth_date, :date
    add_column :users, :description, :text, null: true
    add_column :users, :avatar, :string
    add_column :users, :level, :string
    add_column :users, :privacy_level, :string
  end
end
