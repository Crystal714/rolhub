class CreateGameType < ActiveRecord::Migration[5.1]
  def change
    create_table :game_types do |t|
      t.string :name
    end
    add_index :game_types, :name, unique: true
  end
end
