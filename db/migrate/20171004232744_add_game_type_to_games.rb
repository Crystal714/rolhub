class AddGameTypeToGames < ActiveRecord::Migration[5.1]
  def change
    add_reference :games, :game_type, foreign_key: true, index: true
  end
end
