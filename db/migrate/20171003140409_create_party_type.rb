class CreatePartyType < ActiveRecord::Migration[5.1]
  def change
    create_table :party_types do |t|
      t.string :name
    end
    add_index :party_types, :name, unique: true
  end
end
