class AddVisibilityToGroups < ActiveRecord::Migration[5.1]
  def change
    add_reference :groups, :visibility, foreign_key: true, index: true
  end
end
