class AddPartyTypeToGroups < ActiveRecord::Migration[5.1]
  def change
    add_reference :groups, :party_type, foreign_key: true, index: true
  end
end
