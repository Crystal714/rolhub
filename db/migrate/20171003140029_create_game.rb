class CreateGame < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.string :name
      t.text :description
      t.string :dice
    end
    add_index :games, :name, unique: true
  end
end
